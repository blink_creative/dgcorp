<?php
/*
Plugin Name: Formidable to MailChimp
Description: Save and update MailChimp contacts from your Formidable forms
Version: 1.02.01
Plugin URI: http://formidablepro.com/
Author URI: http://strategy11.com
Author: Strategy11
*/

// Settings
require_once(dirname( __FILE__ ) .'/models/FrmMlcmpSettings.php');


//Controllers
require_once(dirname( __FILE__ ) .'/controllers/FrmMlcmpAppController.php');
require_once(dirname( __FILE__ ) .'/controllers/FrmMlcmpSettingsController.php');

$obj = new FrmMlcmpAppController();
$obj = new FrmMlcmpSettingsController();

include_once(dirname( __FILE__ ) .'/helpers/FrmMlcmpAppHelper.php');
$obj = new FrmMlcmpAppHelper();


