<?php

/**

 * Template Name: About

 *

 * A custom page template with sidebar.

 *

 * @package WordPress

 * @subpackage Starkers

 * @since Starkers 3.0

 */



Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>



    <!--//Content-->

    

    <div id="frame-content" class="subpage">

        <div class="hidden-sm hidden-xs">

        	<div class="row">

                <div class="col-lg-2 col-md-2">

                	<img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/watermark_sidebar.jpg" alt="Deluca Property Group Logo"/>

                </div>

                <div class="col-lg-6 col-md-8">

                    <h2><?php the_field("introduction")?></h2>

                </div>

                <div class="col-lg-2"></div>

            </div>

            <div class="row">

                <div class="col-lg-2 col-md-2"></div>

                <div class="col-lg-6 col-md-6">

					<?php $feature_image = get_field('feature_image');	?>

                    <?php if($feature_image){?>

                        <img class="img-responsive" src="<?php echo $feature_image['url']; ?>" alt="<?php echo $feature_image['alt']; ?>" />

                    <?php } ?>

                </div>

                <div class="col-lg-2 col-md-2 about-aside">

                    <?php the_field("image_caption")?>

                   	<div class="rounded-divider small"></div>

					<?php if(get_field("group_image_quote")){?>
                        <blockquote><?php the_field("group_image_quote")?></blockquote>
                        <cite><?php the_field("group_image_citation")?></cite>
                    <?php } ?>

                </div>

            </div>

            <div class="row">

                <div class="col-lg-2 col-md-2"></div>

                <div class="col-lg-6 col-md-6">

                    <div class="rounded-divider small"></div>

           		</div>

            </div>

			<?php 

                $people = get_field("profile");

                $num_people = count($people);

                $i = 1;

                foreach($people as $person){

            ?>

            <div class="row">

                <div class="col-lg-2 col-md-2"></div>

                <div class="col-lg-2 col-md-2">

                	<?php $profile_image = $person["profile_image"];?>

                        <img class="img-responsive" src="<?php echo $profile_image['url']; ?>" alt="<?php echo $profile_image['alt']; ?>" />

                </div>

                <div class="col-lg-4 col-md-4">

                    <h3><?php echo $person["name"]?>,<br/><?php echo $person["position"]?></h3>

                    <div class="rounded-divider tiny-thin"></div>

                    <?php echo $person["overview"]?>

                    <div class="rounded-divider tiny-thin"></div>

                    <h4>Qualifications</h4>

                    <ul>

                        <?php $qualifications = $person["qualifications"];

                            foreach($qualifications as $qualificaiton){

                        ?>

                            <li><?php echo $qualificaiton["qualification_item"];?></li>

                        <?php }?>

                    </ul>

					<?php if($i != $num_people){?>

                        <div class="rounded-divider small"></div>

                    <?php } ?>

                </div>

            </div>

			<?php $i++; };?>

            <div class="row">

                <div class="col-lg-2 col-md-2"></div>

                <div class="col-lg-6 col-md-6">

                    <div class="rounded-divider small"></div>

                    <h2><?php the_field('collaborators_copy');?></h2>

                    <div class="client-list">

                        <div class="row">

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/rothe.gif" alt="Rothe Lowman logo"/>

                                </div>

                            </div>

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/jason-clements.gif" alt="Jackson Clements Burrows logo"/>

                                </div>

                            </div>

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/chambtrian.gif" alt="Chamberlain Javens Architects logo"/>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/360-property.gif" alt="360 Property Group logo"/>

                                </div>

                            </div>

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/kay.gif" alt="Kay &amp; Burton logo"/>

                                </div>

                            </div>

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/blink.gif" alt="Blinkcreative logo"/>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="http://www.dpgcorp.com.au/wp-content/uploads/2014/07/contour.png" alt="Contour logo"/>

                                </div>

                            </div>

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="http://www.dpgcorp.com.au/wp-content/uploads/2014/07/terrain.png" alt="Terrain logo"/>

                                </div>

                            </div>

                            <div class="col-lg-3 col-md-3">

                            	<div class="border">

                                    <img src="http://www.dpgcorp.com.au/wp-content/uploads/2014/07/web-logo-1.png" alt="Rincovitch logo"/>

                                </div>

                            </div>

                        </div>                    







		    </div>

           	</div>

            </div>

        </div>

        <div class="visible-sm visible-xs">

        	<div class="row">

                <div class="col-md-2 col-sm-2 hidden-xs"></div>

                <div class="col-md-8 col-sm-8 col-xs-10">

                    <h2><?php the_field("introduction")?></h2>

                </div>

            </div>

            <div class="row">

                <div class="col-md-2 col-sm-2 hidden-xs"></div>

                <div class="col-md-8 col-sm-8 col-xs-10">

					<?php $feature_image = get_field('feature_image');	?>

                    <?php if($feature_image){?>

                        <img class="img-responsive" src="<?php echo $feature_image['url']; ?>" alt="<?php echo $feature_image['alt']; ?>" />

                    <?php } ?>

                </div>

            </div>

            <div class="row">

                <div class="col-md-2 col-sm-2 hidden-xs"></div>

                <div class="col-md-8 col-sm-8 col-xs-10 about-aside">
                    <?php the_field("image_caption")?>
                    <?php if(get_field("image_quote")){?>
                        <div class="rounded-divider small"></div>
                        <blockquote><?php the_field("image_quote")?></blockquote>
                        <cite><?php the_field("image_quote_citation")?></cite>
                    <?php } ?>
                    <div class="rounded-divider small"></div>

           		</div>

            </div>

            <div class="row">


                <div class="col-md-2 col-sm-2  hidden-xs about-profile-image">

					<?php $group_image = get_field('group_image');	?>

                    <?php if($group_image){?>

                        <img class="img-responsive" src="<?php echo $group_image['url']; ?>" alt="<?php echo $group_image['alt']; ?>" />

                    <?php } ?>
					<?php if(get_field("group_image_citation")){?>
                        <blockquote><?php the_field("group_image_quote")?></blockquote>
                        <cite><?php the_field("group_image_citation")?></cite>
                    <?php } ?>

                </div>

                <div class="col-md-8 col-sm-8 col-xs-10">

                	<?php 

						$people = get_field("profile");

						$num_people = count($people);

						$i = 1;

						foreach($people as $person){

					?>

                        <h3><?php echo $person["name"]?>,<br/><?php echo $person["position"]?></h3>

                        <div class="rounded-divider tiny-thin"></div>

						<?php echo $person["overview"]?>

                        <div class="rounded-divider tiny-thin"></div>

                        <h4>Qualifications</h4>

                        <ul>

                        	<?php $qualifications = $person["qualifications"];

								foreach($qualifications as $qualificaiton){

							?>

                                <li><?php echo $qualificaiton["qualification_item"];?></li>

                            <?php }?>

                        </ul>

                        <?php if($i != $num_people){?>

                            <div class="rounded-divider small"></div>

                        <?php } ?>

                    <?php $i++; };?>

                </div>

            </div>

            <div class="row">

                <div class="col-md-2 col-sm-2 hidden-xs"></div>

                <div class="col-md-8 col-sm-8 col-xs-10">

                    <div class="rounded-divider small"></div>

                    <h2>Committed to delivering highly sought after innovative and iconic projects, DPG collaborates with some of the most talented in the industry.</h2>

                    <div class="client-list">

                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/rothe.gif" alt="Rothe Lowman logo"/>

                                </div>

                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/jason-clements.gif" alt="Jackson Clements Burrows logo"/>

                                </div>

                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/chambtrian.gif" alt="Chamberlain Javens Architects logo"/>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/360-property.gif" alt="360 Property Group logo"/>

                                </div>

                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/kay.gif" alt="Kay &amp; Burton logo"/>

                                </div>

                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/blink.gif" alt="Blinkcreative logo"/>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="http://www.dpgcorp.com.au/wp-content/uploads/2014/07/contour.png" alt="Contour logo"/>

                                </div>

                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="http://www.dpgcorp.com.au/wp-content/uploads/2014/07/terrain.png" alt="Terrain logo"/>

                                </div>

                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">

                            	<div class="border">

                                    <img src="http://www.dpgcorp.com.au/wp-content/uploads/2014/07/web-logo-1.png" alt="Rincovitch logo"/>

                                </div>

                            </div>

                        </div>

                    </div>

           	</div>

            </div>

        </div>

    </div>

    

    <!--//End Content-->



<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer' ) ); ?>