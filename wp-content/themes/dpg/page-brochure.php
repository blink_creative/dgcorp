<?php
/**
 * Template Name: Brochure
 *
 * A custom page template with sidebar.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

    <!--//Content-->
    
    <div id="frame-content" class="subpage gallery-project">
        <div class="row">
            <div class="col-lg-2 col-md-2 hidden-xs hidden-sm">
                <h2><?php the_title();?></h2>
                <?php echo the_field("address"); ?>
                <?php if(get_field("website_address")){?>
                    <div class="rounded-divider small"></div>
                    <h3><a href="<?php echo the_field("website_address"); ?>" target="_blank">View Website</a></h3>
                <?php } ?>
                <div class="rounded-divider small"></div>
                <?php echo the_field("coming_soon"); ?>
                <div class="rounded-divider small larger-bottom"></div>
                <?php echo the_content();?>
            </div>


            <div id="gallery-image" class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                <div id="logo-image">
                    <?php 
                        $image = get_field('logo_image');

                        if( !empty($image) ): ?>

                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <?php endif; ?>
                </div>  

                <div id="download-btn">
                    <a href="<?php the_field('contract_doc'); ?>" target="_blank">Download brochure</a>
                </div>  
                <div class="visible-sm visible-xs">
                    <h2><?php the_title();?></h2>
                    <?php echo the_field("address"); ?>
					<?php if(get_field("website_address")){?>
                        <div class="rounded-divider small"></div>
                        <h3><a href="<?php echo the_field("website_address"); ?>" target="_blank">View Website</a></h3>
                    <?php } ?>
                    <div class="rounded-divider small"></div>
                    <?php echo the_field("coming_soon"); ?>
                    <div class="rounded-divider small larger-bottom"></div>
                    <?php echo the_content();?>
                </div>
                <?php $portrait_no = 1; $projects = get_field('project_images'); foreach($projects as $project){ ?>
                    <?php 
                        $project_image = $project['project_image']; 
                        $project_width = $project_image['width'];
                        $project_height = $project_image['height'];
                    ?>
                    <?php if($project_width > $project_height){?>
                        <?php if($portrait_no == 2){ ?>
                            </div>
                        <?php $portrait_no = 1;} ?>                            	
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col">
                                <img class="img-responsive" src="<?php echo $project_image['sizes']['large_project_landscape']; ?>" alt="<?php echo $project_image['alt']; ?>" />
                            </div>
                        </div>
                    <?php } else if($project_width < $project_height) {?>
                        <?php if($portrait_no == 1){; ?>
                            <div class="row">
                        <?php } ?>
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <img class="img-responsive" src="<?php echo $project_image['sizes']['large_project_portrait']; ?>" alt="<?php echo $project_image['alt']; ?>" />
                        </div>
                        <?php if($portrait_no == 2){ 
                            $portrait_no = 1; 
                        } ?>
                        <?php $portrait_no++; } ?>
                

				<?php };?>

            </div>

       </div>

    </div>

    

    <!--//End Content-->
    

<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer' ) ); ?>