<!DOCTYPE HTML>
<html><head>

<!--//Meta-->

<title><?php wp_title( '|' ); ?></title>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--//End Meta-->

<!--//CSS-->

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" />
<link href='http://fonts.googleapis.com/css?family=Dosis:500,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/layout.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/favicon.ico"/>

<!--//Scripts-->

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/jquery-core.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-easing.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-quicksand.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-cycle.js"></script>
    
    <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
    <![endif]-->
    
<!--//End Scripts-->

<!--//End CSS-->

<!--//Wordpress Head-->

<?php wp_deregister_script('jquery'); wp_head(); ?>

<!--//End Wordpress Head-->

<!--//GA-->
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-53124312-1', 'auto');
      ga('send', 'pageview');
    
    </script>

<!--//End GA-->

</head>

<body <?php body_class(); ?>>
    
<div class="container">
	    
    <!--//Header-->
    
    <div id="frame-header">
        <div class="row">
            <div id="site-logo" class="col-lg-2 col-md-2 col-sm-2 col-xs-10">
                <div class="rounded-divider hidden-xs"></div>
                <a href="<?php echo site_url(); ?>" title="Link to homepage"><img src="<?php bloginfo('template_url'); ?>/images/logo-main.jpg" alt="Deluca Property Group Logo" width="74" height="39" /></a>
                <div class="rounded-divider"></div>
            </div>
            <div id="frame-navigation" class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                <div class="rounded-divider hidden-xs"></div>
                <ul class="navigation-main nav nav-pills hidden-xs">                
                	<?php $nav_options = array(
						'container'       => '',
						'items_wrap'      => '%3$s'
					); ?>
					<?php wp_nav_menu($nav_options);?>
                </ul>
                <ul class="navigation-main visible-xs">
                	<?php $nav_options = array(
						'container'       => '',
						'items_wrap'      => '%3$s'
					); ?>
					<?php wp_nav_menu($nav_options);?>
                </ul>
                <div class="by-line">
                	<h1 class="hidden-xs">Innovation + Quality = Excellence</h1>
                </div>
                <div class="rounded-divider"></div>
            </div>
         </div>
    </div>
    
    <!--//End Header-->
