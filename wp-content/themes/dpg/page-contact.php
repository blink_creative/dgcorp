<?php

/**

 * Template Name: Contact

 *

 * A custom page template with sidebar.

 *

 * @package WordPress

 * @subpackage Starkers

 * @since Starkers 3.0

 */



Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>



    <!--//Content-->

    

    <div id="frame-content" class="subpage contact">

        <div class="row">

            <div id="contact-details" class="col-lg-2 col-md-2 col-sm-2 col-xs-3">

                <h1><?php the_field('section_1_title')?>+</h1>

				<?php the_field('section_1_copy')?>

                <div class="rounded-divider small hidden-xs"></div>

            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">

                <div id="map"></div>

                <div class="rounded-divider small  hidden-xs"></div>

            </div>

        </div>

        <div class="row">

            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs header-block">

                <h1><?php the_field('section_2_title')?>+</h1>

				<?php the_field('section_2_copy')?>

                <div class="rounded-divider small"></div>

            </div>

            <div class="visible-xs col-xs-10">

                <div class="rounded-divider"></div>

                <h1><?php the_field('section_2_title')?>+</h1>

				<?php the_field('section_2_copy')?>

                <div class="rounded-divider small"></div>

            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">

                <form action="http://blinkcreative.createsend.com/t/r/s/tuniir/" method="post">

                    <div class="float-left">

                        <input onblur="if (value=='') value='First Name'" onfocus="if (value =='First Name') value=''" value="First Name" name="cm-f-jkuldrd" type="text" />

                        <input onblur="if (value=='') value='Last Name'" onfocus="if (value =='Last Name') value=''" value="Last Name" name="cm-f-jkuldrh" type="text" />

                        <input onblur="if (value=='') value='E-mail'" onfocus="if (value =='E-mail') value=''" value="E-mail" name="cm-tuniir-tuniir" type="email" required />

                        <input onblur="if (value=='') value='Phone'" onfocus="if (value =='Phone') value=''" value="Phone" name="cm-f-jkuldrk" type="text" />

                        <button type="submit">Send+</button>

                    </div>

                    <div class="float-left">

                        <select name="cm-f-jkuldru">

                        	<option>Investors</option>

                        	<option>Owner Occupier</option>

                        </select>

                        <select name="cm-f-jkuldyl">

                        	<option>$350K - $400K</option>

                        	<option>$400K - $450K</option>

                        	<option>$450K - $500K</option>

                        	<option>$500K - $600K</option>

                        	<option>$600K - $700K</option>

                        	<option>$700K - $800K</option>

                        	<option>$800K - $900K</option>

                        	<option>$900K - $1M</option>

                            <option>$1M+</option>

                        </select>

                        <select name="cm-f-jkuldyr" >

                            <?php 

                            $project_options = array(

                                'posts_per_page'   => -1,

                                'orderby'          => 'post_date',

                                'order'            => 'DESC',

                                'post_type'        => 'br_projects',

                                'post_status'      => 'publish'

                            ); 

                            $project_array = get_posts($project_options);

                            $i = 1;

                            foreach($project_array as $post): setup_postdata( $post ); ?>

                                <?php 

                                    $terms = wp_get_post_terms($post->ID,"project-categories");

                                    $category = "";

        

                                    foreach($terms as $term){

                                        $category .= $term->slug." ";

                                    }

        

                                ?>

                                <option><?php the_title();?></option>

                            <?php $i++; endforeach; wp_reset_postdata();?>                    

                        </select>

                        <div class="checkbox-container">

                            <label for="subscribe-one">Subscribe to DPG updates</label>

                            <input id="subscribe-one" checked="checked" name="cm-ol-tuniil" type="checkbox" />

                            <div class="clear"></div>

                        </div>

                    </div>

                    <div class="float-left">

                        <textarea name="cm-f-jkuldyy" placeholder="Comments"></textarea>

					</div>

                    <div class="clear"></div>

                </form>            

                <div class="rounded-divider small hidden-xs"></div>

            </div>

            <div class="visible-xs col-xs-10">

                <form class="row">

                    <fieldset>

                        <div class="float-left col-xs-5">

                            <input onblur="if (value=='') value='First Name'" onfocus="if (value =='First Name') value=''" value="First Name" name="cm-f-jkuldrd" type="text" />

                            <input onblur="if (value=='') value='Last Name'" onfocus="if (value =='Last Name') value=''" value="Last Name" name="cm-f-jkuldrh" type="text" />

                            <input onblur="if (value=='') value='E-mail'" onfocus="if (value =='E-mail') value=''" value="E-mail" name="cm-tuniir-tuniir" type="email" required />

                            <input onblur="if (value=='') value='Phone'" onfocus="if (value =='Phone') value=''" value="Phone" name="cm-f-jkuldrk" type="text" />

                        </div>

                        <div class="float-left col-xs-5">

                            <select name="cm-f-jkuldru">

                                <option>Investors</option>

                                <option>Owner Occupier</option>

                            </select>

                            <select name="cm-f-jkuldyl">

                                <option>$350K - $400K</option>

                                <option>$400K - $450K</option>

                                <option>$450K - $500K</option>

                                <option>$500K - $600K</option>

                                <option>$600K - $700K</option>

                                <option>$700K - $800K</option>

                                <option>$800K - $900K</option>

                                <option>$900K - $1M</option>

                                <option>$1M+</option>

                            </select>

                            <select name="cm-f-jkuldyr" >

                                <?php 

                                $project_options = array(

                                    'posts_per_page'   => -1,

                                    'orderby'          => 'post_date',

                                    'order'            => 'DESC',

                                    'post_type'        => 'br_projects',

                                    'post_status'      => 'publish'

                                ); 

                                $project_array = get_posts($project_options);

                                $i = 1;

                                foreach($project_array as $post): setup_postdata( $post ); ?>

                                    <?php 

                                        $terms = wp_get_post_terms($post->ID,"project-categories");

                                        $category = "";

            

                                        foreach($terms as $term){

                                            $category .= $term->slug." ";

                                        }

            

                                    ?>

                                    <option><?php the_title();?></option>

                                <?php $i++; endforeach; wp_reset_postdata();?>                    

                            </select>

                            <div class="checkbox-container">

                                <label for="subscribe-2">DPG Website - Subscribers</label>

                                <input id="subscribe-2" checked="checked" name="cm-ol-tuniil" type="checkbox" /> 

                                <div class="clear"></div>

                            </div>

                        </div>

                        <div class="float-left col-xs-10">

                            <textarea name="cm-f-jkuldyy" placeholder="Comments"></textarea>

                            <button type="submit">Subscribe</button>

                        </div>

                    </fieldset>

                </form>

                <div class="rounded-divider small hidden-xs"></div>

            </div>

        </div>

        <div class="row">

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-10">

                <div class="rounded-divider hidden-lg hidden-md hidden-sm"></div>

                <h1><?php the_field('section_3_title')?>+</h1>

				<?php the_field('section_3_copy')?>

                <div class="rounded-divider small hidden-lg hidden-md hidden-sm"></div>

            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10">

                <form action="http://blinkcreative.createsend.com/t/r/s/tuniil/" method="post">

                    <fieldset>

                        <input onblur="if (value=='') value='First Name'" onfocus="if (value =='First Name') value=''" value="First Name" name="cm-f-jkuldty" type="text" />

                        <input onblur="if (value=='') value='Last Name'" onfocus="if (value =='Last Name') value=''" value="Last Name" name="cm-f-jkuldtj" type="text" />

                        <input onblur="if (value=='') value='E-mail'" onfocus="if (value =='E-mail') value=''" value="E-mail" name="cm-tuniil-tuniil" type="email" required />

                       <button>Send+</button>

                    </fieldset>

                </form>

            </div>

        </div>

    </div>

    

    <!--//End Content-->

    

   <script type="application/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>

    <script type="application/javascript">

        google.maps.event.addDomListener(window, 'load', init);

        function init() {

            var mapOptions = {

                zoom: 17,

                center: new google.maps.LatLng(-37.801451, 144.986729), 

                styles: [{featureType:"landscape",stylers:[{saturation:-100},{lightness:65},{visibility:"on"}]},{featureType:"poi",stylers:[{saturation:-100},{lightness:51},{visibility:"off"}]},{featureType:"road.highway",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"road.arterial",stylers:[{saturation:-100},{lightness:30},{visibility:"on"}]},{featureType:"road.local",stylers:[{saturation:-100},{lightness:40},{visibility:"on"}]},{featureType:"transit",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]/**/},{featureType:"administrative.locality",stylers:[{visibility:"off"}]},{featureType:"administrative.neighborhood",stylers:[{visibility:"on"}]/**/},{featureType:"water",elementType:"labels",stylers:[{visibility:"on"},{lightness:-25},{saturation:-100}]},{featureType:"water",elementType:"geometry",stylers:[{hue:"#ffff00"},{lightness:-25},{saturation:-97}]}]

            };



            var mapElement = document.getElementById('map');

            var map = new google.maps.Map(mapElement, mapOptions);

            marker = new google.maps.Marker({

                map:map,

                animation: google.maps.Animation.DROP,

                position: new google.maps.LatLng(-37.801451, 144.986729),

                icon: '<?php bloginfo('template_url'); ?>/images/marker.png'

              });

        }

    </script>

    

<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer' ) ); ?>