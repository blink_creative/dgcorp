<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <!--//Content-->
    
    <div id="frame-content" class="subpage blog-single">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                <?php
                    if(has_post_thumbnail()){
                        echo the_post_thumbnail("large",array('class' => 'img-responsive'));
                    };
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
                <h2><?php the_title(); ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
                <?php the_content(); ?>
            </div>
        </div>
    </div>

    <!--//End Content-->

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer' ) ); ?>