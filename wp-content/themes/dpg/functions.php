<?php
	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
 	 * @package 	WordPress
 	 * @subpackage 	Starkers
 	 * @since 		Starkers 4.0
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */

	require_once( 'external/starkers-utilities.php' );

	/* ========================================================================================================================
	
	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	
	register_nav_menus(array('primary' => 'Primary Navigation'));
	register_nav_menus(array('footer' => 'Footer Navigation'));
	
	/* ========================================================================================================================
	
	Excerpt Length
	
	======================================================================================================================== */

	
	/* ========================================================================================================================
	
	Actions and Filters
	
	======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'starkers_script_enqueuer' );

	add_filter( 'body_class', array( 'Starkers_Utilities', 'add_slug_to_body_class' ) );
	
	/* ========================================================================================================================

	Image Sizes

	======================================================================================================================== */

	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'project_thumbnail', 101,91, false); 
		add_image_size( 'project_logo', 217,100, false); 
		add_image_size( 'news_homepage_thumbnail', 217,171, true); 
		add_image_size( 'medium_project', 520, 9999, false); 
		add_image_size( 'large_project_portrait', 447, 660, false); 
		add_image_size( 'large_project_landscape', 909, 9999, false); 
		add_image_size( 'project_overview', 216, 216, true); 
		add_image_size( 'blog_small_overview', 216, 247, true); 
		add_image_size( 'blog_large_overview', 448, 247, true); 
		add_image_size( 'project_thumbnail_large', 448, 247, array('center','center')); 
	} 

	/* ========================================================================================================================
	
	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
	======================================================================================================================== */
	
	/*--Taxonomies--*/
	
	add_action( 'init', 'create_my_taxonomies', 0 );
	function create_my_taxonomies() {
		register_taxonomy(
			'project-categories',
			'br_projects',
			array(
				'labels' => array(
					'name' => 'Project Type',
					'add_new_item' => 'Add New Project Type',
					'new_item_name' => "New Project Type"
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true,
				'rewrite' => array(
					'slug' => 'project',
				),
			)
		);
	}		
		
	/*--Custom Post Types--*/
	
	add_action( 'init', 'create_post_type' );
	function create_post_type() {
		register_post_type( 'br_projects',
			array(
				'labels' => array(
					'name' => __( 'Projects' ),
					'singular_name' => __( 'Project' ),
					'add_new' => 'Add Project',
					'edit_item' => 'Edit Projects',
					'new_item' => 'New Project',
					'view_item' => 'View Project',
					'search_items' => 'Search Projects',
					'not_found' => 'No Projects found',
					'not_found_in_trash' => 'No Projects found in Trash'
				),
			'menu_position' => 5,
			'supports' => array('thumbnail','title','editor'),
			'public' => true,
			'has_archive' => 'project',
			'rewrite' => array('slug' => 'projects'),
			'menu_icon' => get_stylesheet_directory_uri() . '/images/project.png',
			'taxonomies' => array( 'project-categories' )
			)
		);
		add_filter('post_type_link', 'project_term_permalink', 10, 4);
		function project_term_permalink($post_link, $post, $leavename, $sample)
		{
			if ( false !== strpos( $post_link, 'project-categories' ) ) {
				$project_categories = get_the_terms( $post->ID, 'project-categories' );
				if($project_categories){
					$post_link = str_replace('%project-categories%', array_pop($project_categories)->slug, $post_link );
				}
			}
			return $post_link;
		}
	}

	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */

	/**
	 * Add scripts via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function starkers_script_enqueuer() {
		/*
		wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ) );
		wp_enqueue_script( 'site' );

		wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );
		*/
	}	

	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
		<?php endif;
	}