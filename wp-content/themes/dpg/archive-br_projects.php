<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts() 
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header') ); ?>

<!--//Splash-->

	<div id="frame-splash-category">
		<div class="wrapper">
			<div id="category-feature">
				<img src="<?php bloginfo('template_url'); ?>/images/overview/safety-cabinets.png" alt="Feature Category" />
			</div>
			<div id="category-introduction">
				<h1>Dangerous Goods Storage Cabinets</h1>
				<h2>When only the best will do for the internal storage of your chemicals</h2>
			</div>
		</div>
	</div>

<!--//End Splash-->

<!--//Content-->

<div id="frame-content">
	<div id="category">
		<div class="wrapper">
			<div id="category-select">
				<h3>Select Category</h3>
				<ul>
					<li id="flammable-liquid-storage-cabinets">
						<a href="flammable-liquid-storage-cabinets">Flammable Liquid Storage Cabinets</a>
					</li>
					<li id="corrosive-storage-cabinets-metal">
						<a href="corrosive-storage-cabinets-metal">Corrosive Storage Cabinets - Metal</a>
					</li>
					<li id="corrosive-storage-cabinets-polyethylene">
						<a href="corrosive-storage-cabinets-polyethylene">Corrosive Storage Cabinets - Polyethylene</a>
					</li>
					<li id="toxic-storage-cabinets">
						<a href="toxic-storage-cabinets">Toxic Substances Cabinets</a>
					</li>
					<li id="oxidizing-agent-storage-cabinets">
						<a href="oxidizing-agent-storage-cabinets">Oxidizing Agent Storage Cabinets</a>
					</li>
					<li id="organic-peroxide-storage-cabinets">
						<a href="organic-peroxide-storage-cabinets">Organic Peroxide Storage Cabinets</a>
					</li>
					<li id="flammable-solid-storage-cabinets">
						<a href="/safety-cabinet/class-4-dangerous-goods-cabinets/">Flammable Solid Storage Cabinets</a>
					</li>
					<li id="spontaneously-combustible-cabinets">
						<a href="/safety-cabinet/class-4-dangerous-goods-cabinets/">Spontaneously Combustible Cabinets</a>
					</li>
					<li id="dangerous-when-wet-storage-cabinets">
						<a href="/safety-cabinet/class-4-dangerous-goods-cabinets/">Dangerous When Wet Storage Cabinets</a>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
		<div class="grey-shadow"></div>
		<div id="about">
			<div class="wrapper">
				<h2 class="float-left">Pratt Safety Systems is proud to introduce our new range of Indoor Dangerous Goods Storage Cabinets with new and updated features.</h2>
				<div class="about-copy float-right">
					<p>Pratt Safety Systems has been manufacturing and distributing our cabinets throughout Australia and the Pacific region for over 30 years. Pratt Safety was the first company to introduce these cabinets into Australia and has continued under its current family ownership in maintaining its high standards of products and service.</p>
					<p>All of our cabinets are designed and manufactured to comply with all of the relevant Australian Standards. Refer to the respective Standards for further specifications and conditions of use.</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="white-shadow"></div>
		<div id="range-features" class="wrapper">
			<h3>Features of New Range</h3>
			<div id="category-gallery">
				<div id="feature-image">
					<div id="feature-text">
						<h4>Sequential Self Closing Doors</h4>
						<p>All two door cabinets incorporate Pratt's unique and improved sequential self-closing door action, providing hands free closing and ensures doors close in the correct sequence.</p>
					</div>
					<img src="<?php bloginfo('url'); ?>/wp-content/uploads/2013/02/sc-feature.jpg" alt="Feature Image SC"/>
				</div>
				<div class="gallery-image">
					<img src="<?php bloginfo('url'); ?>/wp-content/uploads/2013/02/sc-one.jpg" alt="Gallery Image One" />
					<h5>Recessed Handle</h5>
					<p>New recessed handle with key lock to eliminate potential damage from heavy knocks.</p>
				</div>
				<div class="gallery-image">
					<img src="<?php bloginfo('url'); ?>/wp-content/uploads/2013/02/sc-two.jpg" alt="Gallery Image Two" />
					<h5>Self Latching Door</h5>
					<p>Spring loaded 3 point self latching door mechanism for fault free latching action.</p>
				</div>
				<div class="gallery-image end">
					<img src="<?php bloginfo('url'); ?>/wp-content/uploads/2013/02/sc-three.jpg" alt="Gallery Image Three" />
					<h5>Heavy Duty Packaging</h5>
					<p>The unique fully enclosed heavy duty honeycomb core cardboard packaging is mounted on a custom made wooden pallet, providing greater protection of cabinets during storage and transport.</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="grey-shadow"></div>
		<div id="new-features">
			<div class="wrapper">
				<div class="column-one">
					<h3>New Features</h3>
					<ul>
						<li>
							<h4>Heavy Duty Packaging</h4>
							<p>The unique fully enclosed heavy duty honeycomb core cardboard packaging is mounted on a custom made wooden pallet, providing greater protection of cabinets during storage and transport.</p>
						</li>
						<li>
							<h4>Powder Coated Paint Finish</h4>
							<p>Internal and external powder coated finish for improvedchemical resistance.</p>
						</li>
						<li>
							<h4>Recessed Handle</h4>
							<p>New recessed handle with key lock to eliminate potential damage from heavy knocks.</p>
						</li>
						<li>
							<h4>Keyed Alike</h4>
							<p>2 Keys supplied with each cabinet. (Cabinets with locking handles only)</p>
						</li>
						<li>
							<h4>Self Latching Door</h4>
							<p>Spring loaded 3 point self latching door mechanism for fault freelatching action.</p>
						</li>
						<li>
							<h4>Slide Control Arm</h4>
							<p>All single door cabinets now includea slide control arm to prevent the door from being over extended.</p>
						</li>
					</ul>
				</div>
				<div class="column-two">
					<h3>General Structure</h3>
					<ul>
						<li>
							<h4>50mm Vent Bungs</h4>
							<p>Fitted into side walls for venting of cabinets where required. Also includes a proven flash arrester in each vent with sealed enclosurein cavity between walls. Each vent is supplied with resealable metal caps.</p>
						</li>
						<li>
							<h4>Double Steel Walls</h4>
							<p>Constructed using 1.2mm thick,double steel walls with 40mm air gap between walls to provide thermal insulation.</p>
						</li>
						<li>
							<h4>150mm Deep Liquid Tight Sump</h4>
							<p>For containment of leakage or spillage.</p>
						</li>
						<li>
							<h4>Static Earthing Connection</h4>
							<p>Now also including earthing wire with each cabinet to help reduce the potential buildup of static electricity, a known source of ignition.</p>
						</li>
						<li>
							<h4>Classification Labels</h4>
							<p>All appropriate class labels, capacity labels, identification and instruction labels are applied to each cabinet.</p>
						</li>
					</ul>
				</div>
				<div class="column-three">
					<h3>Shelves</h3>
					<ul class="shadow">
						<li>
							<h4>Heavy Duty Perforated Steel</h4>
							<p>1.6mm thick perforated galvanised steel shelves provide added strength and free air movement within the cabinet (All shelves are pre-fittedto cabinets and additional shelves are availableif required).</p>
						</li>
						<li>
							<h4>Fully Adjustable Shelves</h4>
							<p>90mm shelving increments for large cabinets and 45mm for medium and small cabinets.</p>
						</li>
					</ul>
					<h3>Doors</h3>
					<ul>
						<li>
							<h4>Self Closing Doors</h4>
							<p>All two door cabinets incorporate Pratt’s unique and improved sequential self-closing door action, providing hands free closing and ensures doors close in the correct sequence.</p>
						</li>
						<li>
							<h4>Hydraulic Door Closures</h4>
							<p>With speed control adjustment for a gentle closing action.</p>
						</li>
						<li>
							<h4>Continuous Piano Hinge</h4>
							<p>Continuous piano hinges for reliability of action.</p>
						</li>
						<li>
							<h4>Rounded Corners</h4>
							<p>All doors have rounded corners for added safety.</p>
						</li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>

<!--//End Content-->

<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer' ) ); ?>