/*-------------------------------------------------------------------------
Functions
-------------------------------------------------------------------------*/

function category_sorting(){
	var $applications = $('#gallery-list');
	var $data = $applications.clone();
	var gallery_menu_items = $("#gallery-categories li");
	
	// attempt to call Quicksand on every click
	gallery_menu_items.click(function(e) {
		gallery_menu_items.removeClass("active");
		$(this).addClass("active");
		var filter_type = $(this).attr("id");  
				
		if(filter_type == 'all') {
			var $filteredData = $data.find('li');
		} else {
			var $filteredData = $data.find('li.'+filter_type);
		}

		console.log($filteredData);
	
		// finally, call quicksand
		$applications.quicksand($filteredData, {
			duration: 800,
			easing: 'easeInOutQuad',
			adjustHeight: 'auto'
		});
	});
}


/*-------------------------------------------------------------------------
Calls
-------------------------------------------------------------------------*/

category_sorting();
