<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file 
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>
<!--//Content-->
<div id="frame-content" class="subpage news">
	<div class="hidden-xs hidden-sm">
		<div class="row news-blocks">
			<div id="blog-navigation" class="col-lg-2 col-md-2 col-sm-2 hidden-xs"> <span>
				<?php posts_nav_link(' /','NEWER','OLDER'); ?>
				</span>
				<h1>News &amp; Media+</h1>
				<p>An overview of the most recent news and media featuring DPG projects and collaborators.</p>
				<p>Subscribe to the DPG newsletter, to be the first to know about upcoming projects and to keep informed on the latest news.</p>
			</div>
			<?php if ( have_posts() ): 
				$current_articles = 0; 
				while ( have_posts() ) : the_post(); 
					$overview_size = get_field("post_overview_size");
					$video_url = get_field("video");
					$video_id = substr( $video_url, strrpos( $video_url, '/' )+1 );
					$media_link = get_field("media_attachment");
					$date = get_the_date();
						
					if($media_link){
						$link = $media_link;	
					} else {
						$link = esc_url( get_permalink() );
					}
					
					if($video_id){
						$current_articles += 4; 
						if($current_articles > 8){ 
							$current_articles = 0; ?>
							</div>
							<div class="row news-blocks">
								<div class="col-lg-2 col-md-2"></div>
						<?php } ?>
						<div class="col-lg-4 col-md-4 large-news news-video">
							<iframe src="//player.vimeo.com/video/<?php echo $video_id?>?title=0&amp;byline=0&amp;portrait=0" width="100%" height="247" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							<div class="news-inner video">
								<h2><a href="<?php echo $link ?>"><?php echo substr(get_the_title(), 0,35); echo '...';?></a></h2>
								<?php if(get_field("source")){?>
								<h3><?php echo substr(get_field("source"), 0,20);?></h3>
								<?php } ?>
								<div class="rounded-divider tiny-thin"></div>
								<p><?php echo $date?></p>
								<div class="rounded-divider tiny-thin"></div>
								<p><?php echo substr(get_the_excerpt(), 0,125); echo '...';?></p>
							</div>
							<div class="rounded-divider small"></div>
						</div>
						<?php } else {
							if($overview_size == "small"){
								$current_articles += 2; 
								if($current_articles > 7){ 
									$current_articles = 0; ?>
									</div>
									<div class="row news-blocks">
										<div class="col-lg-2 col-md-2"></div>
								<?php } ?>
								<div class="col-lg-2 col-md-2 small-news">
									<?php if(has_post_thumbnail()){?>
									<a href="<?php echo $link ?>"><?php echo the_post_thumbnail("blog_small_overview",array('class' => 'img-responsive')); ?></a>
									<?php } else { ?>
									<a href="<?php echo $link ?>"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/watermark.jpg" alt="Deluca Property Group"/></a>
									<?php } ?>
									<div class="news-inner">
										<h2 class="visible-lg"><a href="<?php echo $link ?>"><?php echo substr(get_the_title(), 0,35); echo '...';?></a></h2>
										<h2 class="hidden-lg"><a href="<?php echo $link ?>"><?php echo substr(get_the_title(), 0,22); echo '...';?></a></h2>
										<?php if(get_field("source")){?>
										<h3><?php echo substr(get_field("source"), 0,20);?></h3>
										<?php } ?>
										<div class="rounded-divider tiny-thin"></div>
										<p><?php echo $date?></p>
										<div class="rounded-divider tiny-thin"></div>
										<p class="visible-lg"><?php echo substr(get_the_excerpt(), 0,55); echo '...';?></p>
										<p class="hidden-lg"><?php echo substr(get_the_excerpt(), 0,45); echo '...';?></p>
									</div>
									<div class="rounded-divider small"></div>
								</div>
						<?php } else if($overview_size == "large") { 
							$current_articles += 4; 
							if($current_articles > 8){ 
								$current_articles = 0; ?>
								</div>
								<div class="row news-blocks">
									<div class="col-lg-2 col-md-2"></div>
									<?php } ?>
									<div class="col-lg-4 col-md-4 large-news news-video"> <a href="<?php echo $link ?>"><?php echo the_post_thumbnail("blog_large_overview",array('class' => 'img-responsive')); ?></a>
										<div class="news-inner">
											<h2 class="visible-lg"><a href="<?php echo $link ?>"><?php echo substr(get_the_title(), 0,100); echo '...';?></a></h2>
											<h2 class="hidden-lg"><a href="<?php echo $link ?>"><?php echo substr(get_the_title(), 0,100); echo '...';?></a></h2>
											<?php if(get_field("source")){?>
											<h3><?php echo substr(get_field("source"), 0,20);?></h3>
											<?php } ?>
											<div class="rounded-divider tiny-thin"></div>
											<p><?php echo $date?></p>
											<div class="rounded-divider tiny-thin"></div>
											<p class="visible-lg"><?php echo substr(get_the_excerpt(), 0,125); echo '...';?></p>
											<p class="hidden-lg"><?php echo substr(get_the_excerpt(), 0,110); echo '...';?></p>
										</div>
										<div class="rounded-divider small"></div>
									</div>
								<?php }  } ?>
						<?php endwhile; ?>
					<?php else: ?>
						<h2>No posts to display</h2>
					<?php endif; ?>
		</div>
	</div>
	<div class="hidden-lg hidden-md">
		<div class="news-blocks">
			<div class="row">
				<div class="col-sm-10 col-xs-10">
					<h1>News &amp; Media+</h1>
					<p>An overview of the most recent news and media featuring DPG projects and collaborators.</p>
					<p>Subscribe to the DPG newsletter, to be the first to know aboutupcoming projects and to keep informed on the latest news.</p>
					<div class="rounded-divider small"></div>
				</div>
			</div>
			<?php if ( have_posts() ): while ( have_posts() ) : the_post(); 
						$overview_size = get_field("post_overview_size");
						$video_url = get_field("video");
						$video_id = substr( $video_url, strrpos( $video_url, '/' )+1 );
						$media_link = get_field("media_attachment");
						$date = get_the_date();
						
						if($media_link){
							$link = $media_link;	
						} else {
							$link = esc_url( get_permalink() );
						}
				?>
			<div class="row">
				<?php if($video_id){?>
				<div class="col-sm-10 col-xs-10 large-news news-video">
					<iframe src="//player.vimeo.com/video/<?php echo $video_id?>?title=0&amp;byline=0&amp;portrait=0" width="100%"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<div class="news-inner video">
						<h2><a href="<?php echo $link ?>"><?php echo substr(get_the_title(), 0,35); echo '...';?></a></h2>
						<?php if(get_field("source")){?>
						<h3><?php echo substr(get_field("source"), 0,20);?></h3>
						<?php } ?>
						<div class="rounded-divider tiny-thin"></div>
						<p><?php echo $date?></p>
						<div class="rounded-divider tiny-thin"></div>
						<p><?php echo substr(get_the_excerpt(), 0,125); echo '...';?></p>
					</div>
					<div class="rounded-divider small"></div>
				</div>
				<?php } else { ?>
				<div class="col-sm-10 col-xs-10 large-news news-video">
					<?php if(has_post_thumbnail()){?>
					<a class="tablet-phone-image" href="<?php echo $link ?>"><?php echo the_post_thumbnail("medium_project",array('class' => 'img-responsive')); ?></a>
					<?php } else { ?>
					<a href="<?php echo $link ?>"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/watermark.jpg" alt="Deluca Property Group"/></a>
					<?php } ?>
					<div class="news-inner">
						<h2 class="visible-lg"><a href="<?php echo $link ?>"><?php echo substr(get_the_title(), 0,100); echo '...';?></a></h2>
						<h2 class="hidden-lg"><a href="<?php echo $link ?>"><?php echo substr(get_the_title(), 0,100); echo '...';?></a></h2>
						<?php if(get_field("source")){?>
						<h3><?php echo substr(get_field("source"), 0,20);?></h3>
						<?php } ?>
						<div class="rounded-divider tiny-thin"></div>
						<p><?php echo $date?></p>
						<div class="rounded-divider tiny-thin"></div>
						<p class="visible-lg"><?php echo substr(get_the_excerpt(), 0,150); echo '...';?></p>
						<p class="hidden-lg"><?php echo substr(get_the_excerpt(), 0,125); echo '...';?></p>
					</div>
					<div class="rounded-divider small"></div>
				</div>
				<?php }  ?>
			</div>
			<?php endwhile; else: ?>
			<h2>No posts to display</h2>
			<?php endif; ?>
		</div>
	</div>
</div>
<!--//End Content-->
<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer') ); ?>
