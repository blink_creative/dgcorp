<?php
/**
 * Template Name: Projects
 *
 * A custom page template with sidebar.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

    <!--//Content-->
    
    <div id="frame-content" class="subpage gallery-overview">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
            	<h1>DPGcorp Projects</h1>
                <ul id="gallery-categories">
                    <li id="now-selling">Now Selling</li>
                    <li id="under-construction">Under Construction</li>
                    <li id="completed">Completed</li>
                    <li id="future">Future</li>
                    <li id="all">All</li>
                </ul>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                <ul id="gallery-list" class="gallery-list">
                    <?php 
                    $project_options = array(
                        'posts_per_page'   => -1,
                        'orderby'          => 'post_date',
                        'order'            => 'DESC',
                        'post_type'        => 'br_projects',
                        'post_status'      => 'publish'
                    ); 
                    $project_array = get_posts($project_options);
                    $i = 1;
                    foreach($project_array as $post): setup_postdata( $post ); ?>
                        <?php 
                            $terms = wp_get_post_terms($post->ID,"project-categories");
                            $category = "";
                            foreach($terms as $term){
                                $category .= $term->slug." ";
                            }
                        ?>
                        <li data-id="id-<?php echo $i ?>" data-type="<?php echo $category ?>" class="<?php echo $category ?> float-left">
                            <a class="hidden-xs" href="<?php the_permalink();?>"><?php echo the_post_thumbnail("project_overview",array('class' => 'img-responsive')); ?></a>
                            <a class="visible-xs" href="<?php the_permalink();?>"><?php echo the_post_thumbnail("blog_large_overview",array('class' => 'img-responsive')); ?></a>
                            <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                            <div class="rounded-divider tiny-thin"></div>
                            <?php echo the_field("overview_copy");?>
                            <div class="rounded-divider tiny-thin"></div>
                            <a href="<?php the_permalink();?>">Read More+</a>
                        </li>
                    <?php $i++; endforeach; wp_reset_postdata();?>                    
                </ul>
                <div class="rounded-divider"></div>
                <p>All images, views and diagrams are indicative of artists' impressions only. All content is subject to copyright and are owned by Deluca Property Group (DPGcorp). For further information please <a href="http://www.dpgcorp.com.au/contact-dpgcorp/" target="_blank">contact DPG Corp today</a>.</p>
            </div
            >
        </div>
    </div>
    
    <!--//End Content-->
<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer' ) ); ?>