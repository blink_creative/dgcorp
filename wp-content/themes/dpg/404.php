<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <!--//Content-->
    
    <div id="frame-content" class="subpage blog-single">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
                <h2>Error - No page found.</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
                <p>Please use the navigation above to get yourself back on track.</p>
            </div>
        </div>
    </div>

    <!--//End Content-->

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>