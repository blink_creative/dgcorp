<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header') ); ?>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    
    <!--//Content-->
    
    <div id="frame-content" class="subpage gallery-project">
        <div class="row">
            <div id="gallery-navigation" class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                <span class="previous"><?php echo previous_post_link('%link', '&lt;');?></span>
                <span>/</span>
                <span class="next"><?php echo next_post_link('%link', '&gt;');?></span>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('<p class="breadcrumbs">','</p>');
                } ?>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 hidden-xs hidden-sm">
                <h2><?php the_title();?></h2>
                <?php echo the_field("address"); ?>
                <?php if(get_field("website_address")){?>
                    <div class="rounded-divider small"></div>
                    <h3><a href="<?php echo the_field("website_address"); ?>" target="_blank">View Website</a></h3>
                <?php } ?>
                <div class="rounded-divider small"></div>
                <?php echo the_field("coming_soon"); ?>
                <div class="rounded-divider small larger-bottom"></div>
                <?php echo the_content();?>
				<?php if(get_field("collaborators")){?>
                    <div class="rounded-divider small"></div>
                    <h3>Collaborators</h3>
					<?php echo the_field("collaborators"); ?>
                <?php } ?>
              <div class="rounded-divider small"></div>
                <h3><a href="/dpg/contact/">Enquire +</a></h3>
                <div class="rounded-divider small"></div>
                <h3 id="share"><span>Share +</span> <a href="mailto:friendsemail@domain.com?subject=<?php the_title();?>&amp;body=Check%20out%20this%20project%20I%20just%20found%20on%20DPGcorp.com.au:%20<?php the_permalink();?>"><img src="<?php bloginfo('template_url'); ?>/images/icon-mail.jpg" alt="Email A Friend"/></a><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>"><img src="<?php bloginfo('template_url'); ?>/images/icon-facebook.jpg" alt="Email A Friend"/></a><a href="http://twitter.com/share?url=<?php the_permalink();?>"><img src="<?php bloginfo('template_url'); ?>/images/icon-twitter.jpg" alt="Email A Friend"/></a><a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink()?>"><img src="<?php bloginfo('template_url'); ?>/images/icon-pintrest.jpg" alt="Email A Friend"/></a></h3>
                <div class="project-logo">
                <?php $project_logo = get_field('project_logo');	?>
                <?php if($project_logo){?>
                    <img src="<?php echo $project_logo['sizes']['project_logo']; ?>" alt="<?php echo $project_logo['alt']; ?>" />
                    <div class="hidden"><?php echo $project_logo['sizes']['project_logo']; ?></div>
                <?php } ?>
                </div>
            </div>
            <div class="visible-sm hidden-xs col-sm-2"></div>
            <div id="gallery-image" class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                <div class="visible-sm visible-xs">
                    <h2><?php the_title();?></h2>
                    <?php echo the_field("address"); ?>
					<?php if(get_field("website_address")){?>
                        <div class="rounded-divider small"></div>
                        <h3><a href="<?php echo the_field("website_address"); ?>" target="_blank">View Website</a></h3>
                    <?php } ?>
                    <div class="rounded-divider small"></div>
                    <?php echo the_field("coming_soon"); ?>
                    <div class="rounded-divider small larger-bottom"></div>
                    <?php echo the_content();?>
                </div>
                <?php $portrait_no = 1; $projects = get_field('project_images'); foreach($projects as $project){ ?>
                    <?php 
                        $project_image = $project['project_image']; 
                        $project_width = $project_image['width'];
                        $project_height = $project_image['height'];
                    ?>
                    <?php if($project_width > $project_height){?>
                        <?php if($portrait_no == 2){ ?>
                            </div>
                        <?php $portrait_no = 1;} ?>                            	
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col">
                                <img class="img-responsive" src="<?php echo $project_image['sizes']['large_project_landscape']; ?>" alt="<?php echo $project_image['alt']; ?>" />
                            </div>
                        </div>
                    <?php } else if($project_width < $project_height) {?>
                        <?php if($portrait_no == 1){; ?>
                            <div class="row">
                        <?php } ?>
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <img class="img-responsive" src="<?php echo $project_image['sizes']['large_project_portrait']; ?>" alt="<?php echo $project_image['alt']; ?>" />
                        </div>
                        <?php if($portrait_no == 2){ 
                            $portrait_no = 1; 
                        } ?>
                        <?php $portrait_no++; } ?>
                <?php } if(get_field('latitude')&&get_field('longitude')){?>
                	<div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-10 col">
                        	<div data-latitude="<?php the_field('latitude');?>" data-longitude="<?php the_field('longitude');?>" <?php if(get_field('google_map_marker')){?>data-marker="<?php the_field('google_map_marker');?>"<?php } else {?>data-marker="<?php bloginfo('template_url'); ?>/images/marker.png"<?php }?> id="project-map"></div>
                        </div>
                    </div>
                
                
                    <script type="application/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
                    <script type="application/javascript">
					
							var latitude = $("#project-map").attr("data-latitude");
							var longitude = $("#project-map").attr("data-longitude");
							var marker = $("#project-map").attr("data-marker");
						
							google.maps.event.addDomListener(window, 'load', init);
							function init() {
								var mapOptions = {
									zoom: 17,
									center: new google.maps.LatLng(latitude, longitude), 
									styles: [{featureType:"landscape",stylers:[{saturation:-100},{lightness:65},{visibility:"on"}]},{featureType:"poi",stylers:[{saturation:-100},{lightness:51},{visibility:"off"}]},{featureType:"road.highway",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"road.arterial",stylers:[{saturation:-100},{lightness:30},{visibility:"on"}]},{featureType:"road.local",stylers:[{saturation:-100},{lightness:40},{visibility:"on"}]},{featureType:"transit",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]/**/},{featureType:"administrative.locality",stylers:[{visibility:"off"}]},{featureType:"administrative.neighborhood",stylers:[{visibility:"on"}]/**/},{featureType:"water",elementType:"labels",stylers:[{visibility:"on"},{lightness:-25},{saturation:-100}]},{featureType:"water",elementType:"geometry",stylers:[{hue:"#ffff00"},{lightness:-25},{saturation:-97}]}]
								};
					
								var mapElement = document.getElementById('project-map');
								var map = new google.maps.Map(mapElement, mapOptions);
								marker = new google.maps.Marker({
									map:map,
									animation: google.maps.Animation.DROP,
									position: new google.maps.LatLng(latitude, longitude),

									icon: marker

								  });

							}

                    </script>

				<?php };?>

		



                <div class="visible-sm visible-xs">

                    <?php if(get_field("collaborators")){?>

                        <div class="rounded-divider small"></div>

                        <h3>Collaborators</h3>

                        <?php echo the_field("collaborators"); ?>

                    <?php } ?>

                    <div class="rounded-divider small"></div>

                    <h3>Enquire +</h3>

                    <div class="rounded-divider small"></div>

                    <h3>Share +</h3>

                    <div class="project-logo">

                        <?php $project_logo = get_field('project_logo');	?>

                        <?php if($project_logo){?>

                            <img src="<?php echo $project_logo['sizes']['project_logo']; ?>" alt="<?php echo $project_logo['alt']; ?>" />

                            <div class="hidden"><?php echo $project_logo['sizes']['project_logo']; ?></div>

                        <?php } ?>

                    </div>

                </div>

            </div>

       </div>

    </div>

    

    <!--//End Content-->

    

	<?php endwhile; ?>

    

<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer' ) ); ?>