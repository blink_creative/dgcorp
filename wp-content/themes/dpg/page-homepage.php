<?php
/*** Template Name: Homepage
 *
 * A custom page template with sidebar.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

    <!--//Content-->
    
    <div id="frame-content">
        <div class="row hidden-xs">
        	<div class="col-lg-2 col-md-2 col-sm-2">
                <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/watermark_sidebar.jpg" alt="DPG Corp Logo"/>
            </div>
        	<div class="col-lg-8 col-md-8 col-sm-8">
            	<div class="blocks">
                	<div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5">
                        	<div class="row">
                            	<div class="image-block tall col-lg-10 col-md-10 col-sm-10">
									<div class="inset">
										<h2><a href="<?php echo site_url(); ?>/projects/hive-apartments-thornbury/">Hive Thornbury</a></h2>
										<div class="rounded-divider tiny"></div>
                                        <h3>Now Selling!</h3>
										<p>630 High Street, Thornbury</p>
									</div>
									<a href="<?php echo site_url(); ?>/projects/hive-apartments-thornbury/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/placeholder-blocks/150514-Hive-Exterior.png" alt="Placeholder"/></a>
                                </div>
                            </div>
                        </div>
                    	<div class="col-lg-5 col-md-5 col-sm-5">
                            <div id="news-rotator" class="row">
                                <div class="image-block small col-lg-5 hidden-md hidden-sm hidden-xs">
									<?php
										query_posts("cat=-8&showposts=5");
										while (have_posts()) : the_post();
										$news_excerpt = substr(get_the_excerpt(), 0,40)."...";
										?>
                                            <a title="<?php echo $news_excerpt?>" href="<?php the_permalink();?>">
												<?php
                                                    if(has_post_thumbnail()){
                                                        echo the_post_thumbnail("news_homepage_thumbnail",array('class' => 'img-responsive'));
                                                    };
                                                ?>
                                            </a>
									<?php endwhile;  wp_reset_postdata();?>
                                </div>
                                <div class="text-block small col-lg-5 col-md-10 col-sm-10">
                                	<div class="inner-block">
                                        <h2>News</h2>
                                        <div class="rounded-divider tiny"></div>
                                        <p id="caption">
											<?php
                                                query_posts("cat=-8&showposts=1");
                                                while (have_posts()) : the_post();
													$news_excerpt = substr(get_the_excerpt(), 0,40)."...";
													echo $news_excerpt;
												endwhile;  wp_reset_postdata();
											?>
                                        </p>
                                        <div id="news-pagination"></div>
                                    </div>
                                </div>
                        	</div>
                            <div class="row">
                                <div class="image-block wide col-lg-10 col-md-10 col-sm-10">
                                	<div class="inset">
                                    	<h2><a href="<?php echo site_url(); ?>/projects/17-lisson-grv/">Lisson Hawthorn</a></h2>
                                        <div class="rounded-divider tiny hidden-sm"></div>
                                        <p><h3>SOLD OUT!</h3></p>
                                    </div>
                                	<a href="<?php echo site_url(); ?>/projects/17-lisson-grv/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/placeholder-blocks/soldoutlisson.png" alt="Placeholder"/></a>
                                </div>
							</div>  
                        	<div class="row">
                            	<div class="text-block large col-lg-10 col-md-10 col-sm-10">
                                	<div class="inner-block hidden-sm">
                                        <h2>Deluca Property Group</h2>
                                        <div class="rounded-divider tiny"></div>
                                        <h3>DPG Corp is an acclaimed private development company committed to high quality, designer lifestyle apartments.</h3>
                                        <div class="rounded-divider small larger-padding"></div>
                                        <p>For over a decade, DPG Corp has applied a philosophy that combines unique and exclusive inner city locations with fresh and contemporary design solutions to create urban spaces where people simply love to live.</p>
                                        <a class="hidden-md hidden-sm read-more" href="<?php echo site_url(); ?>/about/">Read <br/>More+</a>
                                    </div>
                                    <a href="<?php echo site_url(); ?>/about/" class="inner-block visible-sm">
                                        <h3>DPG Corp is an acclaimed private development company committed to high quality, designer lifestyle apartments.</h3>
                                        <div class="rounded-divider small larger-padding hidden-sm"></div>
                                        <p>For over a decade, DPG Corp has applied a philosophy that combines unique and exclusive inner city locations with fresh and contemporary design solutions.</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                	<div class="row">
                        <div class="image-block large-wide col-lg-10 col-md-10 col-sm-10 margin-block">
							<div class="inset">
								<h2><a href="<?php echo site_url(); ?>/projects/204-high-st-preston/">Seed Preston</a></h2>
								<div class="rounded-divider tiny"></div>
                                <h3>Builder Appointed</h3>
								<p>Construction on Seed Preston starting!</p>
							</div>
							<a href="<?php echo site_url(); ?>/projects/204-high-st-preston/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/placeholder-blocks/seedprestonwide.png" alt="Placeholder"/></a>
                        </div>
                    </div>
                	<div class="row">
                        <div class="image-block large-wide col-lg-10 col-md-10 col-sm-10">
                            <div class="inset">
                                <h2><a href="<?php echo site_url(); ?>/projects/oban-south-yarra/">Oban South Yarra</a></h2>
                                <div class="rounded-divider tiny"></div>
                                <h3>Only 2 Luxury Apartments remaining</h3>
                                <p>An exclusive release of 5 luxury apartments.</p>
                            </div>
                            <a href="<?php echo site_url(); ?>/projects/oban-south-yarra/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/placeholder-blocks/image-large-wide.jpg" alt="Placeholder"/></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="subscribe-box">
                                <form action="http://blinkcreative.createsend.com/t/r/s/tuniil/" method="post">
                                    <fieldset>
                                        <legend class="col-lg-5 col-md-4 col-sm-4">Subscribe to our newsletter</legend>
                                        <div class="col-lg-2 col-md-3 col-sm-3">
                                            <input onblur="if (value=='') value='First Name'" onfocus="if (value =='First Name') value=''" value="First Name" name="cm-f-jkuldty" type="text" />
                                        </div>
                                        <div class="col-lg-2 col-md-3 col-sm-3">
                                            <input onblur="if (value=='') value='E-mail'" onfocus="if (value =='E-mail') value=''" value="E-mail" name="cm-tuniil-tuniil" type="email" required />
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-1">
                                            <button>Subscribe</button>
                                        </div>
                                    </fieldset>
                                </form>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="visible-xs">
            <div class="blocks">
                <div class="row">
                    <div class="image-block tall col-xs-10">
                        <div class="inset">
                            <h2><a href="<?php echo site_url(); ?>/projects/hive-apartments-thornbury/">Hive Thornbury</a></h2>
                            <div class="rounded-divider tiny"></div>
							<h3>Now Selling!</h3>
							<p>630 High Street, Thornbury</p>
                        </div>
                        <a href="<?php echo site_url(); ?>/projects/hive-apartments-thornbury/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/placeholder-blocks/150514-Hive-Exterior.png" alt="Placeholder"/></a>
                    </div>
                </div>
                <div class="row">
                    <div class="image-block wide col-xs-10">
						<div class="inset">
							<h2><a href="<?php echo site_url(); ?>/projects/17-lisson-grv/">Lisson Hawthorn</a></h2>
							<div class="rounded-divider tiny hidden-sm"></div>
							<h3>SOLD OUT!</h3>
						</div>
						<a href="<?php echo site_url(); ?>/projects/17-lisson-grv/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/placeholder-blocks/soldoutlisson.png" alt="Placeholder"/></a>
                    </div>
                </div>  
                <div class="row">
                    <div class="text-block large col-xs-10">
                        <div class="inner-block">
                            <h2>Deluca Property Group</h2>
                            <div class="rounded-divider tiny"></div>
                            <h3>DPG Corp is an acclaimed private development company committed to high quality, designer lifestyle apartments.</h3>
                            <div class="rounded-divider small larger-padding"></div>
                            <p>For over a decade, DPG Corp has applied a philosophy that combines unique and exclusive inner city locations with fresh and contemporary design solutions to create urban spaces where people simply love to live.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="image-block large-wide col-xs-10 margin-block">
                        <div class="inset">
                            <h2><a href="<?php echo site_url(); ?>/projects/204-high-st-preston/">Seed Preston</a></h2>
                            <div class="rounded-divider tiny"></div>
                            <h3>Builder Appointed</h3>
							<p>Construction on Seed Preston starting!</p>
                        </div>
                        <a href="<?php echo site_url(); ?>/projects/204-high-st-preston/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/placeholder-blocks/seedprestonwide.png" alt="Placeholder"/></a>
                    </div>
                </div>
                <div class="row">
                    <div class="image-block large-wide col-xs-10">
                        <div class="inset">
                            <h2><a href="<?php echo site_url(); ?>/projects/oban-south-yarra/">Oban South Yarra</a></h2>
                            <div class="rounded-divider tiny"></div>
                            <h3>Only 2 Luxury Apartments remaining</h3>
                            
                        </div>
                        <a href="<?php echo site_url(); ?>/projects/oban-south-yarra/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/placeholder-blocks/image-large-wide.jpg" alt="Placeholder"/></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                        <div class="subscribe-box">
                            <form action="http://blinkcreative.createsend.com/t/r/s/tuniil/" method="post">
                                <fieldset>
                                    <legend class="col-lg-5 col-md-4 col-sm-4">Subscribe to our newsletter</legend>
                                    <div class="col-lg-2 col-md-3 col-sm-3">
                                        <input onblur="if (value=='') value='First Name'" onfocus="if (value =='First Name') value=''" value="First Name" name="cm-f-jkuldty" type="text" />
                                    </div>
                                    <div class="col-lg-2 col-md-3 col-sm-3">
                                        <input onblur="if (value=='') value='E-mail'" onfocus="if (value =='E-mail') value=''" value="E-mail" name="cm-tuniil-tuniil" type="email" required />
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1">
                                        <button>Subscribe</button>
                                    </div>
                                </fieldset>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
       	</div>
    </div>
    
    <!--//End Content-->
    
    <script type="application/javascript">
		$("#news-rotator .image-block").cycle({
			pager:"#news-pagination",
			timeout: 5000,
			before:     function() {
				$('#caption').html(this.title);
			}
		});
	</script>
<?php Starkers_Utilities::get_template_parts( array('parts/shared/html-footer' ) ); ?>