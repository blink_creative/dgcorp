<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dpgcorp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?6)EC]x>Hh+bZ`/5$#W3y@ySgbO+LA3<%2a9pALvvHy.|FFB&wH^#tx>)R,XnZMQ');
define('SECURE_AUTH_KEY',  'B$HAm<f%!h];o4be=Cx=>|x1K-++bqHl-o3h(4QFolo$m|KrtIYh`Vr8ZnJ|@2nR');
define('LOGGED_IN_KEY',    '+4{U<VO5LEb~0%<LUn+>b=x8,X9p-QiN1|SG&9Rtx-aHV$gTEFVM,(1ISutRF{tP');
define('NONCE_KEY',        '`#U?||7!,*2fQ R[l8l:C<[tCMb=+erj^3<y^XV?rwcw9JQ;-R|[$Wj=t*|M-$.r');
define('AUTH_SALT',        '7TQq6D+4Rb#,My-*8d5*kg1^ZfID+tA|&!+e ,Mgg){`9yDwhb:1 zW|YSNJ!dxS');
define('SECURE_AUTH_SALT', '^fC-q;!%],4dBfj8!6s`^)u<odA$y#{x$~]U+7h2eq<(-5MC)&qr->o]/lfs3{?-');
define('LOGGED_IN_SALT',   '(A>x1n(.!e;xG}bI7]M<Xv>s>;Psf@`|@#~mAhX@jx9YCZ{H>P8vfO&|:&w`rPG2');
define('NONCE_SALT',       'VD>>iN%zHsQ0J?8P8B|v`AgrF!x9H/V8JaJ!pD#*GzEz]Y9^c_weG2#QEn@-2~_8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/*--THIS MUST BE ON ALL BLINKCREATIVE WORDPRESS SITES LOCATED ON THE TESTING SERVERS--*/

$dev_account = "jimmy";
$site_folder = "dpgcorp.com.au";

define( 'WP_SITEURL', 'http://web7-pc/'.$dev_account.'/'.$site_folder);
define( 'WP_HOME', 'http://web7-pc/'.$dev_account.'/'.$site_folder);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


define('WP_MEMORY_LIMIT', '100M');